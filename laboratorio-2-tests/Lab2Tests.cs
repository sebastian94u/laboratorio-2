using NUnit.Framework;
using Laboratorio2App;

namespace Laboratorio2Tests
{
  [TestFixture]
  public class Lab2Tests
  {

    private Lab2 _lab2;

    [SetUp]
    public void SetUp()
    {
      _lab2 = new Lab2();
    }

    [Test]
    public void TestEncuentraLetra_CharExiste_ReturnIndex()
    {
      
      char letra = 'u';
      string str = "prueba";

      string resultado = _lab2.EncuentraLetra(letra, str);

      Assert.AreEqual($"Letra está en index {str.IndexOf(letra)}", resultado);
    }

    [Test]
    public void TestEncuentraLetra_CharNoEncontrado_ReturnStringLetraNoEncontrada ()
    {
      char letra = 'x';
      string str = "prueba";

      string resultado = _lab2.EncuentraLetra(letra, str);

      Assert.AreEqual("Letra no encontrada.", resultado);
    }

    [Test]
    public void TestEncuentraLetra_StringVacio_ReturnStringVacio ()
    {
      char letra = 'l';
      string str = "";

      string resultado = _lab2.EncuentraLetra(letra, str);

      Assert.AreEqual("String vacío o null.", resultado);
    }
  }
}
