using System;

namespace Laboratorio2App
{
  public class Lab2
  {
    public string EncuentraLetra(char letra, string str)
    {
      string msj = "";

      char[] letras = str.ToCharArray();

      if (string.IsNullOrEmpty(str))
      {
        msj = "String vacío o null.";
      }
      else
      {
        for (int i = 0; i < letras.Length; i++)
        {
          if (letras[i] == letra)
          {
            msj = $"Letra está en index {i}";
          }
          Console.WriteLine("Posición actual: " + i);
        }
      }

      if (msj == "")
      {
        msj = "Letra no encontrada.";
      }
      return msj;
    }
  }
}
